package be.nicholas.helloclient.web.out;

import feign.Response;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

@SpringBootTest
public class HelloTlsClientTest {

    @Autowired
    private HelloTlsClient helloTlsClient;

    @Autowired
    private InitFileClient initFileClient;

    @Test
    public void hello() {
        Map<String, String> response = helloTlsClient.hello();
        Assertions.assertEquals("hello", response.get("message"));
    }

    @Test
    public void truststore() throws IOException {
        Response response = initFileClient.truststore();
        InputStream inputStream = response.body().asInputStream();

        File targetFile = new File("src/main/resources/targetFile.tmp");

        FileUtils.copyInputStreamToFile(inputStream, targetFile);

        inputStream.close();
        response.close();

    }
}
