package be.nicholas.helloclient.web.out;

import feign.Response;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "init", url = "http://ms-hello:8080", configuration = InitFileClientConfig.class)
public interface InitFileClient {

    @RequestMapping(method = RequestMethod.GET, path = "/hello/truststore")
    Response truststore();

    @RequestMapping(method = RequestMethod.GET, path = "/hello/keystore")
    Response keystore();
}
