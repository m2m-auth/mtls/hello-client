package be.nicholas.helloclient.web.out;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@FeignClient(name = "tls", url = "https://ms-hello", configuration = HelloTlsClientConfig.class)
public interface HelloTlsClient {

    @RequestMapping(method = RequestMethod.GET, path = "/hello")
    Map<String, String> hello();
}
