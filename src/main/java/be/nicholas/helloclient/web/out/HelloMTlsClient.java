package be.nicholas.helloclient.web.out;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@FeignClient(name = "mtls", url = "https://ms-hello:9443", configuration = HelloMTlsClientConfig.class)
public interface HelloMTlsClient {

    @RequestMapping(method = RequestMethod.GET, path = "/hello")
    Map<String, String> hello();
}
