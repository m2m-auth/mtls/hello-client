package be.nicholas.helloclient.web.out;

import feign.Client;
import feign.Feign;
import feign.Response;
import feign.Retryer;
import org.apache.commons.io.FileUtils;
import org.apache.http.conn.ssl.DefaultHostnameVerifier;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@Configuration
public class HelloMTlsClientConfig {

    private final InitFileClient initFileClient;

    public HelloMTlsClientConfig(InitFileClient initFileClient) {
        this.initFileClient = initFileClient;
    }

    private SSLSocketFactory getSSLSocketFactory() {

        String password = "123456";

        try {
            SSLContext sslContext = SSLContextBuilder
                    .create()
                    //.loadTrustMaterial(null, new TrustAllStrategy())
                    //.loadTrustMaterial(null, new TrustSelfSignedStrategy())
                    .loadTrustMaterial(getTrustStore(), password.toCharArray())
                    .loadKeyMaterial(getKeyStore(), password.toCharArray(), password.toCharArray())
                    .build();
            return sslContext.getSocketFactory();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

    @Bean(name = "mtls")
    public Feign.Builder feignBuilder() {
        return Feign.builder()
                .retryer(Retryer.NEVER_RETRY)
                .client(new Client.Default(getSSLSocketFactory(), new DefaultHostnameVerifier()));
    }

    private File getTrustStore() throws IOException {
        Response response = initFileClient.truststore();
        InputStream inputStream = response.body().asInputStream();

        File targetFile = new File("truststore.jks");

        FileUtils.copyInputStreamToFile(inputStream, targetFile);

        inputStream.close();
        response.close();

        return targetFile;
    }

    private File getKeyStore() throws IOException {
        Response response = initFileClient.keystore();
        InputStream inputStream = response.body().asInputStream();

        File targetFile = new File("keystore.jks");

        FileUtils.copyInputStreamToFile(inputStream, targetFile);

        inputStream.close();
        response.close();

        return targetFile;
    }
}
