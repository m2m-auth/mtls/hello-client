package be.nicholas.helloclient.task;

import be.nicholas.helloclient.web.out.HelloMTlsClient;
import be.nicholas.helloclient.web.out.HelloTlsClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@EnableScheduling
public class HelloTask {

    private final static Logger log = LoggerFactory.getLogger(HelloTask.class);

    private final HelloTlsClient helloTlsClient;
    private final HelloMTlsClient helloMTlsClient;

    public HelloTask(HelloTlsClient helloTlsClient, HelloMTlsClient helloMTlsClient) {
        this.helloTlsClient = helloTlsClient;
        this.helloMTlsClient = helloMTlsClient;
    }

    @Scheduled(initialDelay = 120000, fixedDelay = 1000 * 60)
    public void tls() {
        Map<String, String> response = helloTlsClient.hello();
        log.info("TLS: {}", response.get("message"));
    }

    @Scheduled(initialDelay = 123000, fixedDelay = 1000 * 60)
    public void mtls() {
        Map<String, String> response = helloMTlsClient.hello();
        log.info("MTLS: {}", response.get("message"));
    }
}
