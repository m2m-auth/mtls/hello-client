FROM openjdk:11

WORKDIR /app

COPY target/hello-client.jar app.jar

CMD ["java", "-Duser.timezone=Europe/Brussels", "-jar", "app.jar"]
